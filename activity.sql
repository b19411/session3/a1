
-- Users

INSERT INTO users (email, password, datetime_created) VALUES ("johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00");

INSERT INTO users (email, password, datetime_created) VALUES ("juandelacruz@gmail.com", "passwordB", "2021-01-01 02:00:00");

INSERT INTO users (email, password, datetime_created) VALUES ("jamesmith@gmail.com", "passwordC","2021-01-01 03:00:00");

INSERT INTO users (email, password, datetime_created) VALUES ("mariadelacruz@gmail.com", "passwordD", "2021-01-01 04:00:00");

INSERT INTO users (email, password, datetime_created) VALUES ("johndoe@gmail.com", "passwordE", "2021-01-01 05:00:00");

-- Posts

INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (1, "First Code", "Hello World!", "2021-01-02 01:00:00");

INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (1, "Second Code", "Hello Earth!", "2021-01-02 02:00:00");

INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (2, "Third Code", "Welcome to Mars!", "2021-01-02 03:00:00");

INSERT INTO posts (user_id, title, content, datetime_posted) VALUES (4, "Fourth Code", "Bye bye solar system!", "2021-01-02 04:00:00");

-- GET ALL THE POST WITH AN AUTHOR ID OF 1

SELECT * FROM posts WHERE user_id = 1;

-- GET ALL THE USER'S EMAIL AND DATETIME OF CREATION.
SELECT email, datetime_created FROM users;

-- UPDATE A POSTS'S CONTENT TO "HELLO TO THE PEOPLE OF THE EARTH!" WHERE ITS INITIAL CONTENT IS "HELLO EARTH!" BY USING THE RECORD'S ID.

UPDATE posts SET content = "Hello to the people of the Earth!" WHERE content = "Hello Earth!";

-- DELETE THE USER WITH AN EMAIL OF "JOHNDOE@GMAIL.COM"

DELETE FROM users WHERE email = "johndoe@gmail.com";